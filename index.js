const start = document.querySelector('.start');
start.addEventListener('click', game);
const card = document.querySelectorAll('.card');
let finalPoint = 0;
function restartPage() {
    console.log("you refreshed the page!!!")
    location.reload();
}


function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;

    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;

        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}



window.addEventListener('load', function () {
    let game = document.querySelector('.game');
    let cards = Array.from(game.getElementsByClassName('card'));
    let shuffledCards = shuffle(cards);

    while (game.firstChild) {
        game.removeChild(game.firstChild);
    }

    shuffledCards.forEach(function (card) {
        game.appendChild(card);
    });
});


function game() {
    start.removeEventListener('click', game);



    let moves = 0;
    let finalmoves = 0;
    let finaltime = 0;
    let prev = '';
    let prevEle;
    let time = 0;
    let status = 0;

    function updateMoves() {
        document.querySelector('.moves').innerHTML = `${moves} moves`;
    }
    const timerfunc = setInterval(() => {
        time++;
        document.querySelector('.times').innerHTML = `Time : ${time} sec`;
    }, 1000);


    function check(element, cur) {
        if (prev == cur && prevEle != element) {
            finalPoint++;
            if (finalPoint == 8) {
                finalmoves = moves;
                finaltime = time;
                clearInterval(timerfunc);
                const game = document.querySelector(".game");
                let custom = document.querySelector(".result");
                let customData = document.querySelector('.result-data');
                setTimeout(() => {
                    transformReverse(element, prevEle);
                    prevEle.style.opacity = "0%";
                    element.style.opacity = "0%";
                    custom.style.display = "block";
                    customData.innerHTML += `<i class="fa-solid fa-shield-halved"></i> <div>Congratulations you completed the game within ${finaltime} secs <br> Score: ${finalmoves}</div> <br> `;
                }, 800);
                for (let index = 0; index < 16; index++) {
                    game.children[index].removeEventListener('click', transform);
                }
            }
            else {
                status = 0;
                prev = '';
                setTimeout(() => {
                    transformReverse(element, prevEle);
                    prevEle.style.opacity = "0%";
                    element.style.opacity = "0%";
                    enable();
                }, 800);
                prevEle.removeEventListener('click', transform);
                element.removeEventListener('click', transform);
            }
        }
        else {
            setTimeout(() => {
                transformReverse(element, prevEle);

                enable();
            }, 800);
            prev = '';
            status = 0;
        }
    }
    function transformReverse(element, prevEle) {
        element.style.transform = '  rotateZ(0deg) rotateY(0deg) rotateZ(0deg)';
        prevEle.style.transform = '  rotateZ(0deg) rotateY(0deg) rotateZ(0deg)';

    }
    function transform() {
        let now = 0;
        if (prev.length < 1 && status == 0) {
            prev = this.className.split(" ")[2];
            prevEle = this;
            now = 1;
        }
        console.log(prev);
        moves++;
        status = 1;
        console.log(moves);
        this.style.transform = '  rotateZ(90deg) rotateY(180deg) rotateZ(90deg)';
        this.style.transition = 'all 1s';
        console.log(prev, this.className.split(" ")[2]);
        updateMoves();
        if (moves > 1 && now == 0) {
            disable();
            check(this, this.className.split(" ")[2]);
        }

    }



    function enable() {

        for (let index = 0; index < card.length; index++) {
            card[index].addEventListener('click', transform);
        }
    }
    function disable() {
        for (let index = 0; index < card.length; index++) {
            card[index].removeEventListener('click', transform);
        }
    }
    enable();

}


















